FROM alpine:3.15.0

LABEL version=v1.2.0

RUN set -x && apk add --no-cache \
      curl \
      rsync \
      python3 \
      py3-pip \
      python3-dev \
      bash \
      coreutils \
      libffi-dev \
      openssl \
      openssl-dev \
      libressl-dev \
      gcc \
      libc-dev \
      make \
      musl-dev \
      cargo \
    && pip3 install awscli docker-compose \
    && curl -L "https://github.com/docker/machine/releases/download/v0.16.2/docker-machine-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-machine \
    && chmod +x /usr/local/bin/docker-machine \
    && curl -L "https://github.com/JonasProgrammer/docker-machine-driver-hetzner/releases/download/3.5.0/docker-machine-driver-hetzner_3.5.0_linux_amd64.tar.gz" | tar xz -C /usr/local/bin

CMD ["/bin/bash"]
